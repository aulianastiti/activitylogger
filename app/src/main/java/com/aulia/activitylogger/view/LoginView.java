package com.aulia.activitylogger.view;

import com.google.android.gms.auth.api.signin.GoogleSignInAccount;

/**
 * Created by Aulia Nastiti on 7/3/2017.
 */

public interface LoginView {

    void onUserLoginFound();
    void onUserLoginNotFound();
    void firebaseAuthResult(GoogleSignInAccount account);

}
