package com.aulia.activitylogger.database;

import android.content.ContentValues;
import android.content.Context;
import android.database.Cursor;
import android.database.sqlite.SQLiteDatabase;
import android.location.Location;

import com.aulia.activitylogger.database.helper.ActivitiesOpenHelper;
import com.aulia.activitylogger.model.Activity;
import com.aulia.activitylogger.helper.LocationHelper;

import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;

/**
 * Created by Aulia Nastiti on 7/2/2017. *
 *
 */

public class ActivityLog {
    Context context;
    SQLiteDatabase database;
    public ActivityLog(Context context){
        this.context = context;
        try {
            database = ActivitiesOpenHelper.open(context);
        }catch (Exception e){

        }
    }

    public static ActivityLog with(Context context){
        return new ActivityLog(context);
    }

    public void addLocal(Activity act){
        Location loc = LocationHelper.with(context).getCurrentLocation();
        try {
            ContentValues data = new ContentValues();
            data.put(ActivitiesOpenHelper.Contract.Column.NAME, act.getName());
            data.put(ActivitiesOpenHelper.Contract.Column.TIME, act.getTime() );
            if(loc!=null) {
                data.put(ActivitiesOpenHelper.Contract.Column.LATITUDE, loc.getLatitude());
                data.put(ActivitiesOpenHelper.Contract.Column.LONGITUDE,loc.getLongitude());
            }
            data.put(ActivitiesOpenHelper.Contract.Column.ACTIVITY, act.getActivity());
            data.put(ActivitiesOpenHelper.Contract.Column.CATEGORY, act.getCategory());
            if(act.getNote()!=null) {
                data.put(ActivitiesOpenHelper.Contract.Column.NOTE, act.getNote());
            }
            database.insert(ActivitiesOpenHelper.Contract.TABLE, null, data);
        }catch(Exception e){
        }
    }
    public void addLocal(List<Activity> activities){
        clear();
        for(Activity a:activities){
            addLocal(a);
        }
    }
    public ArrayList<Activity> getAllActivities(){
        Cursor cursor = database.rawQuery("select * from " + ActivitiesOpenHelper.Contract.TABLE + " ORDER BY datetime(" + ActivitiesOpenHelper.Contract.Column.TIME+ ") DESC" ,null);

        ArrayList<Activity> data = new ArrayList<>();
        if(cursor.moveToFirst()) {
            do {
                Activity temp = new Activity();
                temp.setName(cursor.getString(0));
                temp.setTime(cursor.getString(1));
                temp.setLatitude(cursor.getDouble(2));
                temp.setLongitude(cursor.getDouble(3));
                temp.setActivity(cursor.getString(4));
                temp.setCategory(cursor.getString(5));
                temp.setNote(cursor.getString(6));
                data.add(temp);
            } while (cursor.moveToNext());
        }
        return data;
    }

    public void clear(){
        database.delete(ActivitiesOpenHelper.Contract.TABLE, null, null);
    }
}
