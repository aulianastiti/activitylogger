package com.aulia.activitylogger.database.helper;

import android.content.Context;
import android.database.sqlite.SQLiteDatabase;
import android.database.sqlite.SQLiteOpenHelper;

/**
 * Created by Aulia Nastiti on 7/2/2017.
 *
 */

public class ActivitiesOpenHelper extends SQLiteOpenHelper {
    private static String DB_NAME = "activities.db";
    private static int DB_VERSION = 1;

    public ActivitiesOpenHelper(Context context){
        super(context,DB_NAME, null, DB_VERSION);
    }
    public static SQLiteDatabase open(Context context){
        return new ActivitiesOpenHelper(context).getWritableDatabase();
    }

    public static class Contract{
        public static String TABLE = "activity";
        public static class Column{
            public static String NAME = "name";
            public static String TIME = "time";
            public static String LONGITUDE = "longitude";
            public static String LATITUDE = "latitude";
            public static String ACTIVITY="aktivitas";
            public static String CATEGORY="kategori";
            public static String NOTE="note";
        }
        public static class Rules{
            public static String ID = " INTEGER";
            public static String LOG = " TEXT";
            public static String DATE = " DATE";
            public static String REAL = "REAL";
        }
        public static String SEPARATOR = ",";
    }
    private String structTable(){
        String temp = "CREATE TABLE IF NOT EXISTS "+Contract.TABLE+"("+
                Contract.Column.NAME + Contract.Rules.LOG + Contract.SEPARATOR +
                Contract.Column.TIME + Contract.Rules.DATE + Contract.SEPARATOR +
                Contract.Column.LONGITUDE + Contract.Rules.REAL + Contract.SEPARATOR +
                Contract.Column.LATITUDE + Contract.Rules.REAL + Contract.SEPARATOR +
                Contract.Column.ACTIVITY + Contract.Rules.LOG + Contract.SEPARATOR +
                Contract.Column.CATEGORY+ Contract.Rules.LOG + Contract.SEPARATOR +
                Contract.Column.NOTE + Contract.Rules.LOG +
                ");";
        return temp;
    }
    @Override
    public void onCreate(SQLiteDatabase db) {
        db.execSQL(structTable());
    }

    @Override
    public void onUpgrade(SQLiteDatabase db, int oldVersion, int newVersion) {
        if (newVersion > oldVersion) {
            db.execSQL("DROP TABLE IF EXISTS " + Contract.TABLE);
        }
        onCreate(db);
    }
    @Override
    public void onDowngrade(SQLiteDatabase db, int oldVersion, int newVersion) {
        super.onDowngrade(db, oldVersion, newVersion);
    }
}
