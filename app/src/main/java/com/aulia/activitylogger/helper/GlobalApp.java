package com.aulia.activitylogger.helper;

import android.app.Application;

import com.google.firebase.database.FirebaseDatabase;

/**
 * Created by Aulia Nastiti on 7/2/2017.
 */

public class GlobalApp extends Application {
    private GoogleApiHelper googleApiHelper;
    private static GlobalApp mInstance;

    @Override
    public void onCreate() {
        super.onCreate();
        FirebaseDatabase.getInstance().setPersistenceEnabled(true);
        mInstance = this;
        googleApiHelper = new GoogleApiHelper(mInstance);
    }

    public static synchronized GlobalApp getInstance() {
        return mInstance;
    }

    public GoogleApiHelper getGoogleApiHelperInstance() {
        return this.googleApiHelper;
    }
    public static GoogleApiHelper getGoogleApiHelper() {
        return getInstance().getGoogleApiHelperInstance();
    }
}
