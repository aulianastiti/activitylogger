package com.aulia.activitylogger.helper;

import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.Locale;

/**
 * Created by Aulia Nastiti on 7/2/2017.
 */

public class TimestampHelper {

    public static String convertIndonesian(String timestamp) {
        try {
            if(!timestamp.equals("0000-00-00 00:00:00")) {
                Date e = (new SimpleDateFormat("yyyy-MM-dd HH:mm:ss")).parse(timestamp);
                return (new SimpleDateFormat("EEEE, dd MMMM yyyy - HH:mm", new Locale("in", "ID"))).format(e);
            } else {
                return "No data set";
            }
        } catch (Exception var2) {
            return "No Time Defined";
        }
    }
    public static String getCurrentTime(){
        Date curDate = new Date();
        SimpleDateFormat format = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss");
        String dateToStr = format.format(curDate);
        return dateToStr;
    }
    public static String getCurrentTimeConverted(){
        Date curDate = new Date();
        SimpleDateFormat format = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss");
        String dateToStr = format.format(curDate);
        return  convertIndonesian(dateToStr);
    }
}
