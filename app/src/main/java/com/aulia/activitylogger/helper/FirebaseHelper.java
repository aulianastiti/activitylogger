package com.aulia.activitylogger.helper;

import android.content.Context;

import com.aulia.activitylogger.database.ActivityLog;
import com.aulia.activitylogger.model.Activity;
import com.google.firebase.database.DatabaseReference;
import com.google.firebase.database.FirebaseDatabase;

/**
 * Created by Aulia Nastiti on 7/3/2017.
 */

public class FirebaseHelper {
    public static void add(Context context,Activity act){
        DatabaseReference mDatabase = FirebaseDatabase.getInstance().getReference("activity");
        String id = mDatabase.push().getKey();
        mDatabase.child(id).setValue(act);
        if(!ConnectionDetector.with(context).isConnectingToInternet()){
            ActivityLog.with(context).addLocal(act);
        }
    }
}
