package com.aulia.activitylogger.helper;

import android.content.Context;
import android.content.Intent;
import android.support.annotation.NonNull;
import android.widget.Toast;

import com.aulia.activitylogger.ui.activities.LoginActivity;
import com.aulia.activitylogger.model.User;
import com.google.android.gms.auth.api.Auth;
import com.google.android.gms.auth.api.signin.GoogleSignInAccount;
import com.google.android.gms.common.api.GoogleApiClient;
import com.google.android.gms.common.api.ResultCallback;
import com.google.android.gms.common.api.Status;
import com.google.firebase.auth.FirebaseAuth;

/**
 * Created by Aulia Nastiti on 7/2/2017.
 */

public class SessionManager {
    private TinyDB db;
    private Context context;
    private FirebaseAuth auth;

    private static final String FIRST_NAME ="front_name";
    private static final String LAST_NAME="last_name";
    private static final String EMAIL="email";

    public SessionManager(Context context){
        db = new TinyDB(context);
        auth = FirebaseAuth.getInstance();
        this.context = context;
    }
    public static SessionManager with(Context context){
        return new SessionManager(context);
    }


    public void createSession(GoogleSignInAccount account){
        if(account.getGivenName()!=null)db.putString(FIRST_NAME, account.getGivenName());
        if(account.getFamilyName()!=null)db.putString(LAST_NAME,account.getFamilyName());
        if(account.getEmail()!=null)db.putString(EMAIL,account.getEmail());
    }

    public User getUserProfile(){
        User user = new User();
        user.setFirstName(db.getString(FIRST_NAME));
        user.setLastName(db.getString(LAST_NAME));
        user.setEmail(db.getString(EMAIL));
        return user;
    }
    public void destroySession(){
        if(GoogleApiHelper.getInstance()!=null) {
            Auth.GoogleSignInApi.signOut(GoogleApiHelper.getInstance()).setResultCallback(
                    new ResultCallback<Status>() {
                        @Override
                        public void onResult(Status status) {
                            if(status.isSuccess()){
                                auth.signOut();
                                auth.addAuthStateListener(new FirebaseAuth.AuthStateListener() {
                                    @Override
                                    public void onAuthStateChanged(@NonNull FirebaseAuth firebaseAuth) {
                                        context.startActivity(new Intent(context, LoginActivity.class));

                                    }
                                });
                                db.clear();
                            }else{
                                Toast.makeText(context, "Failed to log out. Try again later", Toast.LENGTH_SHORT).show();
                            }
                        }
                    });

        }

    }
}
