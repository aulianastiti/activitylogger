package com.aulia.activitylogger.ui.fragment;

import android.location.Location;
import android.os.Bundle;
import android.os.Handler;
import android.support.annotation.Nullable;
import android.support.v4.app.Fragment;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.EditText;
import android.widget.Spinner;
import android.widget.TextView;
import android.widget.Toast;

import com.aulia.activitylogger.R;
import com.aulia.activitylogger.helper.FirebaseHelper;
import com.aulia.activitylogger.helper.LocationHelper;
import com.aulia.activitylogger.helper.SessionManager;
import com.aulia.activitylogger.helper.TimestampHelper;
import com.aulia.activitylogger.model.Activity;
import com.aulia.activitylogger.model.User;

import java.sql.Time;

import butterknife.BindView;
import butterknife.ButterKnife;
import butterknife.OnClick;

/**
 * Created by Aulia Nastiti on 7/2/2017.
 */

public class SubmitFragment extends Fragment {
    private Handler timer;
    @BindView(R.id.waktu)
    TextView waktu;
    @BindView(R.id.nama)
    TextView nama;
    @BindView(R.id.inputActivity)
    EditText input_activity;
    @BindView(R.id.category)
    Spinner category_selector;
    @BindView(R.id.inputNote) EditText input_notes;
    User user;

    @Nullable
    @Override
    public View onCreateView(LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        View rootView = inflater.inflate(
                R.layout.fragment_submit, container, false);
        ButterKnife.bind(this,rootView);
        user = SessionManager.with(getActivity()).getUserProfile();
        nama.setText("Nama : " + user.getFirstName()+" "+user.getLastName());
        waktu.setText("Waktu : " + TimestampHelper.getCurrentTimeConverted());


        return rootView;
    }

    @OnClick(R.id.btn_submit)
    public void onClick(){
        if(dataFilled()){
            Location loc = LocationHelper.with(getActivity()).getLastKnownLocation();
            Activity act = new Activity();
            act.setName(user.getFirstName()+" "+user.getLastName());
            act.setTime(TimestampHelper.getCurrentTime());
            act.setNote(input_notes.getText().toString());
            act.setActivity(input_activity.getText().toString());
            act.setCategory(category_selector.getSelectedItem().toString());
            if(loc!=null){
                act.setLongitude(loc.getLongitude());
                act.setLatitude(loc.getLatitude());
            }
            FirebaseHelper.add(getActivity(),act);
            Toast.makeText(getActivity(), "Data berhasil ditambahkan", Toast.LENGTH_SHORT).show();
            input_notes.setText("");
            input_activity.setText("");
            category_selector.setSelection(0);
        }else{
            Toast.makeText(getActivity(), "Data belum lengkap", Toast.LENGTH_SHORT).show();
        }

    }
    public boolean dataFilled(){
        if(!input_activity.equals("") && !category_selector.getSelectedItem().toString().equals("--")){
            return true;
        }
        return false;
    }
}
