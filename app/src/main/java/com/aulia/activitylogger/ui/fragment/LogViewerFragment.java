package com.aulia.activitylogger.ui.fragment;


import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v4.app.Fragment;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import com.aulia.activitylogger.R;
import com.aulia.activitylogger.adapter.LogAdapter;
import com.aulia.activitylogger.database.ActivityLog;
import com.aulia.activitylogger.helper.ConnectionDetector;
import com.aulia.activitylogger.model.Activity;
import com.google.firebase.database.DataSnapshot;
import com.google.firebase.database.DatabaseError;
import com.google.firebase.database.DatabaseReference;
import com.google.firebase.database.FirebaseDatabase;
import com.google.firebase.database.GenericTypeIndicator;
import com.google.firebase.database.ValueEventListener;

import java.util.ArrayList;
import java.util.Comparator;
import java.util.List;
import java.util.Map;

import butterknife.BindView;
import butterknife.ButterKnife;

/**
 * Created by Aulia Nastiti on 7/2/2017.
 */

public class LogViewerFragment extends Fragment implements ValueEventListener {
    private FirebaseDatabase database;
    private DatabaseReference myRef;
    private LogAdapter adapter;

    @BindView(R.id.recyclerView)
    RecyclerView recyclerView;

    @Nullable
    @Override
    public View onCreateView(LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        View rootView = inflater.inflate(
                R.layout.fragment_log_viewer, container, false);
        ButterKnife.bind(this,rootView);
        setUserVisibleHint(true);
        LinearLayoutManager layoutManager = new LinearLayoutManager(getActivity(),LinearLayoutManager.VERTICAL,false);
        recyclerView.setLayoutManager(layoutManager);

        database=FirebaseDatabase.getInstance();
        myRef=database.getReference("activity");
        myRef.addValueEventListener(this);
        return rootView;
    }

    @Override
    public void onDataChange(DataSnapshot dataSnapshot) {
        if(ConnectionDetector.with(getActivity()).isConnectingToInternet()) {
            GenericTypeIndicator<Map<String, Activity>> genericTypeIndicator = new GenericTypeIndicator<Map<String, Activity>>() {
            };
            Map<String, Activity> activityList = dataSnapshot.getValue(genericTypeIndicator);
            if (activityList != null) {
                List<Activity> list = new ArrayList<>(activityList.values());

                ActivityLog.with(getActivity()).addLocal(list);
                list = ActivityLog.with(getActivity()).getAllActivities();
                adapter = new LogAdapter(getActivity(), list);
                recyclerView.setAdapter(adapter);
            }
        }
    }

    @Override
    public void onCancelled(DatabaseError databaseError) {

    }

    @Override
    public void onStart() {
        super.onStart();
        if(!ConnectionDetector.with(getActivity()).isConnectingToInternet()){
            List<Activity> list = ActivityLog.with(getActivity()).getAllActivities();
            adapter = new LogAdapter(getActivity(),list);
            recyclerView.setAdapter(adapter);
        }
    }
}
