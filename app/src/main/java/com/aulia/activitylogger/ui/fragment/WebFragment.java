package com.aulia.activitylogger.ui.fragment;

import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v4.app.Fragment;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.webkit.WebChromeClient;
import android.webkit.WebView;
import android.webkit.WebViewClient;
import android.widget.TextView;

import com.aulia.activitylogger.R;

import butterknife.BindView;
import butterknife.ButterKnife;

/**
 * Created by Aulia Nastiti on 7/2/2017.
 */

public class WebFragment extends Fragment {
    @BindView(R.id.webview)
    WebView webView;
    @BindView(R.id.progress)
    TextView text_progress;

    @Nullable
    @Override
    public View onCreateView(LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        View rootView = inflater.inflate(
                R.layout.fragment_web, container, false);
        ButterKnife.bind(this,rootView);
        webView.getSettings().getJavaScriptEnabled();
        webView.setWebChromeClient(new WebChromeClient(){
            public void onProgressChanged(WebView view, int progress) {
                text_progress.setText("Loading... " + String.valueOf(progress) +"%");
                if(progress == 100)
                    text_progress.setVisibility(View.GONE);
            }
        });
        webView.setWebViewClient(new WebViewClient());
        webView.getSettings().setDomStorageEnabled(true);
        webView.getSettings().setUserAgentString("Android");
        webView.loadUrl("https://mobile.twitter.com/");
        return rootView;
    }
}
