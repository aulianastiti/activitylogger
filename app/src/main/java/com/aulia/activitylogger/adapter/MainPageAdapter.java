package com.aulia.activitylogger.adapter;

import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentManager;
import android.support.v4.app.FragmentPagerAdapter;

import com.aulia.activitylogger.ui.fragment.LogViewerFragment;
import com.aulia.activitylogger.ui.fragment.SubmitFragment;
import com.aulia.activitylogger.ui.fragment.WebFragment;

/**
 * Created by Aulia Nastiti on 7/2/2017.
 */

public class MainPageAdapter extends FragmentPagerAdapter {
    public MainPageAdapter(FragmentManager fm) {
        super(fm);
    }

    @Override
    public Fragment getItem(int position) {
        Fragment fr;
        switch (position){
            case 0:
                fr = new SubmitFragment();
                return fr;
            case 1:
                fr = new WebFragment();
                return fr;
            case 2:
                fr = new LogViewerFragment();
                return fr;
        }
        return null;
    }

    @Override
    public int getCount() {
        return 3;
    }

    @Override
    public CharSequence getPageTitle(int position) {
        switch (position){
            case 0:
                return "Aktivitas";
            case 1:
                return "Webpage";
            case 2:
                return "User";
        }
        return "";
    }
}
