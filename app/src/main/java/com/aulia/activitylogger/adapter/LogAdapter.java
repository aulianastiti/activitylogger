package com.aulia.activitylogger.adapter;

import android.content.Context;
import android.content.Intent;
import android.support.v7.widget.RecyclerView;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.TextView;

import com.aulia.activitylogger.R;
import com.aulia.activitylogger.helper.TimestampHelper;
import com.aulia.activitylogger.model.Activity;

import java.util.ArrayList;
import java.util.List;

/**
 * Created by Aulia Nastiti on 7/3/2017.
 */

public class LogAdapter extends RecyclerView.Adapter<LogAdapter.SimpleViewHolder> {

    private Context context;
    private List<Activity> list;
    private String time;
    private String name;
    private String activity;
    private String category;
    private String notes;
    View view;
    public LogAdapter(Context context, List<Activity> data){
        this.list = data;
        this.context = context;
        Log.e("listview", "adapter created " +String.valueOf(data.size()));
    }

    public static class SimpleViewHolder extends RecyclerView.ViewHolder {
        protected TextView text;

        public SimpleViewHolder(View view){
            super(view);
            text = (TextView) view.findViewById(R.id.log_text);
        }
    }

    @Override
    public SimpleViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        view = LayoutInflater.from(this.context).inflate(R.layout.item_log, parent, false);
        return new SimpleViewHolder(view);
    }

    @Override
    public void onBindViewHolder(final SimpleViewHolder holder, final int position){
        Activity act = list.get(position);
        name = act.getName();
        activity = act.getActivity();
        category = act.getCategory();
        time = act.getTime();
        notes = act.getNote();
        Log.e("listview", time);

        holder.text.setText(
                TimestampHelper.convertIndonesian(time)+"\n"+
                name + " did " + activity + " at "+ category+".\n"+
                "With note : "+notes);
    }

    @Override
    public long getItemId(int position) {
        return position;
    }

    @Override
    public int getItemCount() {
        return list.size();
    }
}
