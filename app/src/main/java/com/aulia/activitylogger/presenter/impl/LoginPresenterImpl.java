package com.aulia.activitylogger.presenter.impl;

import com.aulia.activitylogger.presenter.LoginPresenter;
import com.aulia.activitylogger.view.LoginView;
import com.google.android.gms.auth.api.signin.GoogleSignInAccount;
import com.google.firebase.auth.FirebaseUser;

/**
 * Created by Aulia Nastiti on 7/3/2017.
 */

public class LoginPresenterImpl implements LoginPresenter {
    private LoginView loginView;

    public LoginPresenterImpl(LoginView loginView) {
        this.loginView = loginView;
    }

    @Override
    public void checkLogin(FirebaseUser user) {
        if(user!=null){
            loginView.onUserLoginFound();
        }else{
            loginView.onUserLoginNotFound();
        }
    }

    @Override
    public void firebaseAuthWithGoogle(GoogleSignInAccount account) {
        loginView.firebaseAuthResult(account);
    }
}
