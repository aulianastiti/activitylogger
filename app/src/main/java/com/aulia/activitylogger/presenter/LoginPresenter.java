package com.aulia.activitylogger.presenter;

import android.content.Context;

import com.aulia.activitylogger.model.Activity;
import com.google.android.gms.auth.api.signin.GoogleSignInAccount;
import com.google.firebase.auth.FirebaseUser;

/**
 * Created by Aulia Nastiti on 7/3/2017.
 */

public interface LoginPresenter {
    void checkLogin(FirebaseUser user);
    void firebaseAuthWithGoogle(GoogleSignInAccount account);
}
